/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Miki
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private ComboBox<Object> theaterBox;
    @FXML
    private TextField dateField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField endField;
    @FXML
    private Button listButton;
    @FXML
    private TextField beginField;
    @FXML
    private Button nameSearch;
    @FXML
    private ListView<String> showInfoView;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {   
            getTheaters();
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
    }    
   
    private void getTheaters() throws MalformedURLException, IOException{
        Theater.getTheaters();
        List<String> list = Theater.getNameList();
        for (int i =0; i <list.size(); i++){
            theaterBox.getItems().add(list.get(i));
        }
        
    }
    
    @FXML
    private void listMoviesAction(ActionEvent event) {
        showInfoView.getItems().clear();
        System.out.println("Käsitellään...");
        getShows();
        try{
        List<String> list = Theater.getShowInfo();
        ObservableList<String> showList = FXCollections.observableList(list);
        showInfoView.setItems(showList);
        } catch (NullPointerException ex){
        }
    }
    
    private void getShows() {
        TheaterData td = TheaterData.getTD();
        String place, date, starttime, endtime, showUrl;
        place = null;
        date = "";
        try{
        place = td.getID(theaterBox.getSelectionModel().getSelectedItem().toString());
        date = dateField.getText();
        } catch( NullPointerException ex){
        }
        starttime = beginField.getText();
        endtime = endField.getText();
        if (place ==null) {
            System.err.println("Valitse valikosta haettava teatteri!");
        } 
        else if ((! date.matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d") == true)){
            System.err.println("Päivämäärän formaatti on väärä!");
            // still unexpected behaviour (lists today's movies) at non-legal
            // inputs which fit the format e.g. 70.20.8000
        } 
        else if ((! starttime.matches("\\d\\d\\.\\d\\d")
                && ! starttime.matches("\\d\\d\\:\\d\\d")
                && ! starttime.matches("")== true)){
            System.err.println("Alkamiskellonajan formaatti on väärä!");
        } 
        else if ((! endtime.matches("\\d\\d\\.\\d\\d")
                && ! endtime.matches("\\d\\d\\:\\d\\d")
                && ! endtime.matches("")== true)){
            System.err.println("Loppumiskellonajan formaatti on väärä!");
        } 
        else{
        showUrl = "http://www.finnkino.fi/xml/Schedule/?area="+place+"&dt="+date;
        try {   
            System.out.println("Haetaan elokuvatietoja...");
            Theater.getShows(showUrl, starttime, endtime);
        } catch (IOException ex) {
            System.err.println("Tarkista syöttötietojen oikeellisuus!");
        }
    }
    }

    @FXML
    private void searchByNameAction(ActionEvent event) {
        String title = nameField.getText();
        System.out.println("Käsitellään...");
        Theater.searchByName(title);
        List<String> list = Theater.getMovieShowTimes();
        if (list.size() < 2){
            list.add("Elokuvan nimellä ei löytynyt näytöksiä!");
            ObservableList<String> movieShowList = FXCollections.observableList(list);
            showInfoView.setItems(movieShowList);   
        }
         else{
            ObservableList<String> movieShowList = FXCollections.observableList(list);
            showInfoView.setItems(movieShowList);  
                    }
        }


}
