
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class TheaterData {
    
    private HashMap<String, String> map = new HashMap<String, String>();
    static private TheaterData td =null;
    
    private TheaterData(){
        
    }
    
    public static synchronized TheaterData getTD(){
        if (td == null){
            td = new TheaterData();
            }
        return td;
        }
        
    public void putTheater(String name, String id){
        map.put(name, id);    
    }
     
    public HashMap<String, String> getMap(){
        return map;
    }
    
    public String getID(String key){
        return map.get(key);
        
    }
    
}
