
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class Theater {
   static Document doc; 
   static Document doc2; 
   static Document doc3; 
   static URL url;
   static String name;
   static String ID;
   static ArrayList<Theater> theaterList; 
   static ArrayList<String> showInfo;
   static ArrayList<String> movieShowtimes;

   
public static List<String> getNameList(){
       List<String> names = new ArrayList<String>();
       TheaterData td = TheaterData.getTD();
       HashMap<String, String> map = td.getMap();
       for (Map.Entry<String, String> entry : map.entrySet()){
           names.add(entry.getKey());
       }
       names.sort(String::compareToIgnoreCase);
       return names;
   } 

   public Theater(String n, String id) throws MalformedURLException, IOException{
       name = n;
       ID = id;       
   }
   
   static void getTheaters() throws MalformedURLException, IOException{
       String source = "http://www.finnkino.fi/xml/TheatreAreas/";
       url = new URL(source);
       BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
       InputStream input = url.openStream();
       try {
           DocumentBuilderFactory dbc = DocumentBuilderFactory.newInstance();
           DocumentBuilder db = dbc.newDocumentBuilder();
           doc = db.parse(input);
           doc.getDocumentElement().normalize();
           parseContent();
       } catch (ParserConfigurationException | SAXException | IOException ex) {
           Logger.getLogger(Theater.class.getName()).log(Level.SEVERE, null, ex);
       }
       
   }
   
   static void parseContent() throws IOException{
       NodeList nodes = doc.getElementsByTagName("TheatreArea");
       TheaterData td = TheaterData.getTD();
       theaterList = new ArrayList<Theater>();
       for (int i=0; i< nodes.getLength();i++){
        if (i>2){ // eliminate first two useless name fields
        Node node = nodes.item(i);
        Element element = (Element) node;
        String n = getName(element);
        String id = getID(element);
        theaterList.add(new Theater(n, id));
        td.putTheater(n, id);
        }
        
       }
   }
   
   static String getName(Element element){
       return ((Element)element.getElementsByTagName("Name").item(0)).getTextContent();
   }
   static String getID(Element element){
       return ((Element)element.getElementsByTagName("ID").item(0)).getTextContent();
   }
   static void getShows(String showUrl, String starttime, String endtime) throws MalformedURLException, IOException { 
       URL url = new URL(showUrl);
       BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
       String line;
       System.out.println("Listataan elokuvia...");
       InputStream input = url.openStream();
       starttime = starttime.replace(".", ":");
       endtime = endtime.replace(".", ":");
       try {
           DocumentBuilderFactory dbc2 = DocumentBuilderFactory.newInstance();
           DocumentBuilder db2 = dbc2.newDocumentBuilder();
           doc2 = db2.parse(input);
           doc2.getDocumentElement().normalize();
           parseShowInfo(starttime, endtime);
       } catch (ParserConfigurationException | SAXException | IOException ex) {
           System.err.println("Tarkista syöttötietojen oikeellisuus!");
       }
   }
       
    static void parseShowInfo(String starttime, String endtime) {
        NodeList nodes = doc2.getElementsByTagName("Show");
        showInfo = new ArrayList<String>();
        String off = "[T,Z]+";
        String off2 = "[:]";
        for (int i=0; i< nodes.getLength();i++){
            Node node = nodes.item(i);
            Element element = (Element) node;
            String name = getShowName(element); 
            String start = getShowStart(element);
            String end = getShowEnd(element);
            String auditorium = getAuditorium(element);
            String [] startTokens = start.split(off);
            String [] endTokens = end.split(off);
            String [] finalStartTokens = startTokens[1].split(off2);
            String [] finalEndTokens = endTokens[1].split(off2);
            String actualStart = finalStartTokens[0]+":"+finalStartTokens[1];
            String actualEnd = finalEndTokens[0]+":"+finalEndTokens[1];
            if ("".equals(starttime) && "".equals(endtime)){
                showInfo.add(name+" | Esitysaika: | "+actualStart+" - "+actualEnd+
                         ", "+auditorium);
            }
            else if (actualStart.compareTo(starttime)>=0 && "".equals(endtime)){
                showInfo.add(name+" | Esitysaika: | "+actualStart+" - "+actualEnd+
                         ", "+auditorium);
            }
            else if (actualStart.compareTo(endtime)<=0 && "".equals(starttime)){
                showInfo.add(name+" | Esitysaika: | "+actualStart+" - "+actualEnd+
                         ", "+auditorium);
            }
            else if (actualStart.compareTo(starttime)>=0 && actualStart.compareTo(endtime)<=0){
                showInfo.add(name+" | Esitysaika: | "+actualStart+" - "+actualEnd+
                         ", "+auditorium);
            }
            }
       }
    static String getShowName(Element element){
        return ((Element)element.getElementsByTagName("Title").item(0)).getTextContent();
    }
    static String getShowStart(Element element){
        return ((Element)element.getElementsByTagName("dttmShowStart").item(0)).getTextContent();
    }
    static String getShowEnd(Element element){
        return ((Element)element.getElementsByTagName("dttmShowEnd").item(0)).getTextContent();
    }
    static String getAuditorium(Element element){
        return ((Element)element.getElementsByTagName("TheatreAndAuditorium").item(0)).getTextContent();
    }
    
    public static List<String> getShowInfo(){
        return showInfo;
    }
    
    public static void searchByName(String title){
       try {
           TheaterData td = TheaterData.getTD();
           HashMap<String, String> theaters = td.getMap();
           int i=1;
           int isFound = -1;
           movieShowtimes = new ArrayList<String>();
           movieShowtimes.add("Haettu elokuva: "+ title+
                       "\n----------------------------------------"+
                       "----------------------------------------");
           for (String value : theaters.values()){
                URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area="+value);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                System.out.println("Listataan näytöksiä teattterista " +i+"/18...");
                i++;
                InputStream input = url.openStream();
                DocumentBuilderFactory dbc3 = DocumentBuilderFactory.newInstance();
                DocumentBuilder db3 = dbc3.newDocumentBuilder();
                doc3 = db3.parse(input);
                doc3.getDocumentElement().normalize();
                isFound = parseMovieShows(title, isFound);
           }
       } catch (MalformedURLException ex) {
           System.err.println("Tapahtui virhe. Verkko-osoite saattaa olla muuttanut.");
       } catch (IOException | ParserConfigurationException | SAXException ex) {
           Logger.getLogger(Theater.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    static int parseMovieShows(String title, int isFound){
        NodeList nodes = doc3.getElementsByTagName("Show");  
        String off = "[T,Z]+";
        String off2 = "[:]";
        for (int i=0; i< nodes.getLength();i++){
            Node node = nodes.item(i);
            Element element = (Element) node;
            String name = getMovieName(element);
            String originalName = getOriginalMovieName(element);
            String compareName = name.toLowerCase();
            originalName = originalName.toLowerCase();
            title = title.toLowerCase();
            compareName = compareName.trim();
            originalName = originalName.trim();
            title = title.trim();
            if (compareName.equals(title) || originalName.equals(title)){
                if (isFound == -1){
                    movieShowtimes.add("Esitysajankohdat ( "+name+ " ):");
                    movieShowtimes.add("");
                    isFound++;
                }
                String start = getShowStart(element);
                String end = getShowEnd(element);
                String auditorium = getAuditorium(element);
                String [] startTokens = start.split(off);
                String [] endTokens = end.split(off);
                String [] finalStartTokens = startTokens[1].split(off2);
                String [] finalEndTokens = endTokens[1].split(off2);
                String actualStart = finalStartTokens[0]+":"+finalStartTokens[1];
                String actualEnd = finalEndTokens[0]+":"+finalEndTokens[1];    
                movieShowtimes.add(auditorium+ " | Esitysaika: | "+actualStart+" - "+actualEnd);
            }
            }
           return isFound; 
        }
    
    static String getMovieName(Element element){
        return ((Element)element.getElementsByTagName("Title").item(0)).getTextContent();
    }
    
    static String getOriginalMovieName(Element element){
        return ((Element)element.getElementsByTagName("OriginalTitle").item(0)).getTextContent();
    }
    
    static List<String> getMovieShowTimes(){
        return movieShowtimes;
    }
}
